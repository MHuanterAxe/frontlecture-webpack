import axios from 'axios'

export const baseURL = 'https://jsonplaceholder.typicode.com/'
// export const API_URL = baseURL + 'api/'

const API = axios.create({
  baseURL: baseURL
})

export default API
