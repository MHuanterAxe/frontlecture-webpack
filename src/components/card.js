export default class Card {
  constructor(card) {
    this.title = card.title
    this.img = card.url
  }
  render () {
    return `
      <div class="card">
        <img src="${this.img}" alt="${this.title}">
        <h5>${this.title}</h5>
      </div>
    `
  }
}
