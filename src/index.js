import './styles/style.css'
import './styles/app.sass'
import API from './boot'
import Card from "./components/card";

let cards = []
const app = `<h2 class="app">Лекция</h2>`;

async function fetchData () {
  await API.get('photos/', {
    params: {
      _start: 0,
      _limit: 8
    }
  }). then(response => {
    cards = response.data
  })
}
const mainPage = async () => {
  await fetchData()
  return cards.map(el => {
    return new Card(el).render()
  })
}
mainPage().then(result => {
  render(result)
})
const render = (block) => {
  document.getElementById('app').innerHTML = app;
  if (block) {
    document.getElementById('app').innerHTML += block;
  }
}
render()
